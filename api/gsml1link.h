#ifndef GSML1LINK_H__
#define GSML1LINK_H__

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus 

/****************************************************************************
 *                                 Includes                                 *
 ****************************************************************************/

#include "gsml1serv.h"

#include "gsml1prim.h"
#include "gsml1const.h"
#include "gsml1types.h"

/****************************************************************************
 *                                 Types                                    *
 ****************************************************************************/

/****************************************************************************
 *                              Public Functions                            *
 ****************************************************************************/

/****************************************************************************
 * Function : GsmL1Link_Init
 ************************************************************************//**
 * 
 * Initialise the GSM layer 1 library. 
 *
 * @return
 *    GsmL1_Status_Success or the error code
 *  
 * @ingroup gsml1_api_services
 *
****************************************************************************/
GsmL1_Status_t GsmL1Link_Init( void );

/****************************************************************************
 * Function : GsmL1Link_SetExternalServices
 ************************************************************************//**
 * 
 * Set every services provided to the layer 1 by external libraries
 *
 * @param [in] pL3ProvidedServices 
 *    Points to a structure containing all Layer 3 external services 
 *
 * @param [in] pL2ProvidedServices 
 *    Points to a structure containing all Layer 3 external services 
 *
 * @param [in] pPhyRfProvidedServices 
 *    Points to a structure containing all PhyRf external services 
 *       
 * @return
 *    GsmL1_Status_Success or the error code
 *  
 * @ingroup gsml1_api_services
 *
****************************************************************************/
GsmL1_Status_t GsmL1Link_SetExternalServices( Gsm_L3ProvidedServices_t *pL3ProvidedServices,
                                              Gsm_L2ProvidedServices_t *pL2ProvidedServices,
                                              Gsm_PhyRfProvidedServices_t *pPhyRfProvidedServices );

/****************************************************************************
 * Function : GsmL1Link_GetProvidedServices
 ************************************************************************//**
 * 
 * Return a strucuture containing all service functions pointer provided
 * by the physical link layer library to the external context.
 * 
 * @param [out] pPhyLinkProvidedServices 
 *    Points to a structure of all GSM layer 1 library services function pointer.
 *       
 * @return
 *    GsmL1_Status_Success or the error code
 *  
 * @ingroup gsml1_api_services
 *
****************************************************************************/
GsmL1_Status_t GsmL1Link_GetProvidedServices( Gsm_PhyLinkProvidedServices_t *pPhyLinkProvidedServices );

/****************************************************************************
 * Function : GsmL1Link_Close
 ************************************************************************//**
 * 
 * Close the GSM layer 1 instance and free every allocated ressources.
 * 
 * @return
 *    GsmL1_Status_Success or the error code
 *  
 * @ingroup gsml1_api_services
 *
****************************************************************************/
GsmL1_Status_t GsmL1Link_Close( void );

#ifdef __cplusplus
}
#endif  // extern "C"

#endif // GSML1_H__
