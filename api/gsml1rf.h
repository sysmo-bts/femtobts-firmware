#ifndef GSML1RF_H__
#define GSML1RF_H__

/****************************************************************************
 *                                 Includes                                 *
 ****************************************************************************/
#include <stdint.h>
#include "gsml1serv.h"

/****************************************************************************
 *                                  Defines                                 *
 ****************************************************************************/

#define GSML1RF_NOCHANGE    2 ///< Do not modify RX or TX device in the FPGA side
#define GSML1RF_ENABLE      1 ///< Enable RX or TX device in the FPGA side
#define GSML1RF_DISABLE     0 ///< Disable RX or TX device in the FPGA side


/****************************************************************************
 *                              Public functions                            *
 ****************************************************************************/

/****************************************************************************
 * Function : GsmL1Rf_Init
 ************************************************************************//**
 *
 * Initialize the PhyRf library.
 * 
 * @return
 *    GsmL1_Status_Success or an error code otherwise.
 *
 * @ingroup gsml1_api_phyrf
 *
****************************************************************************/
GsmL1_Status_t GsmL1Rf_Init( void );

/****************************************************************************
 * Function : GsmL1Rf_Close
 ************************************************************************//**
 *
 * Uninitialize the PhyRf library.
 * 
 * @return
 *    GsmL1_Status_Success or an error code otherwise.
 *
 * @ingroup gsml1_api_phyrf
 *
****************************************************************************/
void GsmL1Rf_Close( void );

/****************************************************************************
 * Function : GsmL1Rf_GetProvidedServices
 ************************************************************************//**
 *
 * Used to retrieve a structure of every services provided by the PhyRf layer.
 * 
 * @param [out] pPhyRfProvidedServices
 *    Points to a structure of every services provided by the PhyRf layer
 *
 * @return
 *    GsmL1_Status_Success or an error code otherwise.
 *
 * @ingroup gsml1_api_phyrf
 *
****************************************************************************/
GsmL1_Status_t GsmL1Rf_GetProvidedServices( Gsm_PhyRfProvidedServices_t *pPhyRfProvidedServices );

/****************************************************************************
 * Function : GsmL1Rf_IsInitialized
 ************************************************************************//**
 *
 * Used to determinate if the PhyRf library is initialized
 * 
 * @return
 *    1 if initialized, 0 otherwise
 *
 * @ingroup gsml1_api_phyrf
 *
****************************************************************************/
uint8_t GsmL1Rf_IsInitialized( void );


#ifdef __cplusplus
}
#endif  // extern "C"

#endif	// GSML1PHYRF_H__
