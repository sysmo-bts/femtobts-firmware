#ifndef GSML1SERV_H__
#define GSML1SERV_H__

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus 

/****************************************************************************
 *                                 Includes                                 *
 ****************************************************************************/
#include "gsml1const.h"
#include "gsml1types.h"
#include "gsml1prim.h"

/****************************************************************************
 * Struct : PhyRf_ExternalServices_t
 ************************************************************************//** 
 * 
 * All external services provided from the PhyLink Layer to the PhyRf layer.
 * 
 * @ingroup gsml1_api_phyrf
 *
 ****************************************************************************/
typedef struct PhyRf_ExternalServices_t 
{
    GsmL1_Status_t (*pfRecvBurst)  ( HANDLE hPhyLink, GsmL1_RxBurst_t *pRxBurst );
    GsmL1_Status_t (*pfUpdateTn)   ( HANDLE hPhyLink, uint32_t u32Fn, uint8_t u8Tn );    
                                         
} PhyRf_ExternalServices_t;

/****************************************************************************
 * typedef : Gsm_L3ProvidedServices_t
 ************************************************************************//**
 *
 * Structure which contains a function pointer to every service provided
 * by the Gsm Layer 3 library.
 *   
 * @ingroup gsml1_api_services
 *
 ***************************************************************************/
typedef struct Gsm_L3ProvidedServices
{
    GsmL1_Status_t (*pfMphCnf)( GsmL1_Prim_t *pPrim );
    GsmL1_Status_t (*pfMphInd)( GsmL1_Prim_t *pPrim );     

} Gsm_L3ProvidedServices_t;

/****************************************************************************
 * typedef : Gsm_L2ProvidedServices_t
 ************************************************************************//**
 *
 * Structure which contains a function pointer to every service provided
 * by the Gsm Layer 2 library.
 *   
 * @ingroup gsml1_api_services
 *
 ***************************************************************************/
typedef struct Gsm_L2ProvidedServices_t
{
    GsmL1_Status_t (*pfPhCnf)( GsmL1_Prim_t *pPrim );
    GsmL1_Status_t (*pfPhInd)( GsmL1_Prim_t *pPrim );

} Gsm_L2ProvidedServices_t;

/****************************************************************************
 * typedef : Gsm_PhyLinkProvidedServices_t
 ************************************************************************//**
 *
 * Structure which contains a function pointer to every service provided
 * by the physical link layer library.
 *   
 * @ingroup gsml1_api_services
 *
 ***************************************************************************/
typedef struct Gsm_PhyLinkProvidedServices_t 
{    
    // Services provided to the layer 3
    GsmL1_Status_t (*pfMphReq)( GsmL1_Prim_t *pPrim );
  
    // Services provided to the Layer 2
    GsmL1_Status_t (*pfPhReq)( GsmL1_Prim_t *pPrim );

    // Services provided to the physical radio frequency layer
    GsmL1_Status_t (*pfRecvBurst) ( HANDLE hDevice, GsmL1_RxBurst_t *pRxBurst );
    GsmL1_Status_t (*pfUpdateTn)  ( HANDLE hDevice, uint32_t u32Fn, uint8_t u8Tn );
   
} Gsm_PhyLinkProvidedServices_t;

/****************************************************************************
 * typedef : Gsm_PhyRfProvidedServices_t
 ************************************************************************//**
 *
 * Structure which contains a function pointer to every service provided
 * by the PhyRf library.
 *   
 * @ingroup gsml1_api_services
 *
 ***************************************************************************/
typedef struct Gsm_PhyRfProvidedServices_t
{
    // Services provided from the physical radio frequency layer
    GsmL1_Status_t (*pfInitDevice) ( PhyRf_ExternalServices_t *pPhyRfExternalServices,
                                     GsmL1_DevType_t devType,
                                     GsmL1_FreqBand_t freqBand, 
                                     uint16_t u16Arfcn, 
                                     float fTxPowerLevel,
                                     uint8_t u8NbTsc,
                                     HANDLE hPhyLink,
                                     HANDLE *hPhyRf );
                                     
    void (*pfCloseDevice ) ( HANDLE hPhyRfDevice );

    GsmL1_Status_t (*pfSendBurst ) ( HANDLE hPhyRfDevice, GsmL1_TxBurst_t *pTxBurst );

    GsmL1_Status_t (*pfConfigTs  ) ( HANDLE hPhyRfDevice,
                                     uint8_t u8Tn, 
                                     uint8_t u8EnableRx,
                                     uint8_t u8EnableTxAutoDummy );

    GsmL1_Status_t (*pfSetTxPowerLevel) ( HANDLE hPhyRfDevice,
                                          float fTxPowerLevel );

    GsmL1_Status_t (*pfSetNbTsc) ( HANDLE hPhyRfDevice,
                                   uint8_t u8NbTsc );

    // Services provided by the physical RF layer to the FPGA layer
    void (*pfNewTnIsr) ( void );

} Gsm_PhyRfProvidedServices_t;

#ifdef __cplusplus
}
#endif  // extern "C"

#endif // GSMSERVICES_H__
